from blog.models import Post
from django.shortcuts import render
from django.contrib.auth.decorators import login_required


def get_all(request):
    posts = Post.get_all_post()
    return render(request, 'post_all.html', {'posts': posts})


# @login_required
def add_post(request):
    title = request.POST.get('title', '')
    post_body = request.POST.get('post_body', '')
    author = request.user

    new_post = Post.add_post(title, post_body, author)
    return render(request, 'post.html', {'post': new_post})

