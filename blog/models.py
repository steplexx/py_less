from django.db import models
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned


class Post(models.Model):
    title = models.CharField(max_length=50, help_text="Заголовок поста")
    author = models.ForeignKey(User, help_text="Автор", blank=False, on_delete=models.PROTECT)
    post_body = models.TextField(help_text='Сообщение', blank=True, default='')

    @staticmethod
    def get_all_post():
        posts = Post.objects.all().order_by('id')
        return posts

    @staticmethod
    def add_post(title, post_body, author):
        new_post = Post(
            title=title,
            post_body=post_body,
            author=author
        )
        try:
            new_post.save()
        except Exception:
            return None

        return new_post
