from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Post(models.Model):
    title = models.CharField(max_length=50, help_text="Заголовок поста")
    author = models.ForeignKey(User, help_text="Автор", blank=False, on_delete=models.PROTECT)
    post_body = models.TextField(help_text='Сообщение', blank=True, default='')

