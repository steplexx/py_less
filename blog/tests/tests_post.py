from django.test import TestCase
from blog.models import Post
from django.urls import reverse


class PostTests(TestCase):
    fixtures = ['initial_data.yaml']

    def setUp(self):
        super().setUp()

    def test_user_can_get_posts_list(self):
        response = self.client.get(reverse('post-list'))
        
        self.assertEqual(response.status_code, 200)
        #self.assertContains('')
