from django.test import TestCase
from blog.models import Post
from django.urls import reverse
import mock


class PostTests(TestCase):
    fixtures = ['initial_data.yaml']

    def setUp(self):
        super().setUp()

    def test_user_can_get_posts_list(self):
        response = self.client.get(reverse('post-list'))

        self.assertEqual(response.status_code, 200)

        self.assertContains(response, '<div>Первый пост</div>')
        self.assertContains(response, '<div>Содержание первого поста.</div>')

    def test_user_can_add_post(self):
        data = {
            'title': 'Второй пост',
            'post_body': 'Текст второго поста.'
        }

        self.client.login(username='Alex', password='123')
        response = self.client.post(reverse('add-post'), data=data)

        self.assertContains(response, '<div>Второй пост</div')

        new_post = Post.objects.get(id=2)
        self.assertEqual('Текст второго поста.', new_post.post_body)
        self.assertEqual(1, new_post.author_id)

    def test_error_at_create_new_post(self):
        data = {
            'title': 'Второй пост',
            'post_body': 'Текст второго поста.'
        }

        self.client.login(username='Alex', password='123')
        good_save = Post.save
        Post.save = mock.Mock(
            side_effect=Exception()
        )
        response = self.client.post(reverse('add-post'), data=data)
        self.assertNotContains(response, '<div>Второй пост</div>')
        Post.save = good_save

    def test_create_post_guest(self):
        data = {
            'title': 'Третий пост',
            'post_body': 'Текст третьего поста.'
        }

        response = self.client.post(reverse('add-post'), data=data)
        self.assertEqual(302, response.status_code)
